package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows,columns;
    CellState[][] cellStates;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.cellStates = new CellState[rows][columns];
	}

    @Override
    public int numRows() {
        
        return rows;
    }

    @Override
    public int numColumns() {
       
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
       cellStates[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return cellStates[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid gridCopy = new CellGrid(this.rows, this.columns, null);
        for(int row= 0; row<this.rows; row++){
            for(int col = 0; col<this.columns; col++){
                gridCopy.set(row, col, this.get(row,col));
            }
        }
        return gridCopy;
    }
    
}
